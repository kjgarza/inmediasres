---
layout: default
title: Welcome to In Medias Res
about:
    text: In the Heart of Action, At the Edge of Technology.
twitter_url: https://twitter.com/kriztean/
substack_url: https://kjgarza.substack.com
github_url: https://github.com/kjgarza
medium_url: https://medium.com/@kj.garza/
email: kj.garza@gmail.com

---

